# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :coderspie,
  ecto_repos: [Coderspie.Repo]

# Configures the endpoint
config :coderspie, Coderspie.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "pjOvm3jzVopD5vqeKiX6KpnDd9cR3awLMYgKXXJ4z5DIWpFk52aZe6/wQ+uIUtpy",
  render_errors: [view: Coderspie.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Coderspie.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
